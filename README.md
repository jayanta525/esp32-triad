# Source repo for Triad Spectroscopy Sensor

![triad](./docs/triad.jpg)

### All the embedded source files were build using PlatformIO in VScode.

* https://platformio.org/
* https://code.visualstudio.com/

### Mega2560

![mega2560](./docs/mega.jpg)

* Basic Testing without LEDs.

### ESP32

*Hardware*: ESP32 Development board WT32-SC01

![esp32](./docs/esp32.jpg)


* Template UI 

