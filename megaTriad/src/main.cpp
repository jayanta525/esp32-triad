#include <Arduino.h>
#include "SparkFun_AS7265X.h"
AS7265X sensor;

// #define TEST 0

int readSensor(void);
void promptUser(void);
int validateAction(int action);

void setup()
{
  Serial.begin(9600);
  Serial.println("AS7265x Spectral Triad Example");

#ifndef TEST
  if (sensor.begin() == false)
  {
    Serial.println("Sensor does not appear to be connected. Please check wiring. Freezing...");
    while (1)
      ;
  }
#endif

  Serial.println("\nSensor is detected and ready to use.");
  promptUser();
}

void loop()
{
  while (!Serial.available())
    ;
  char ch = Serial.read();
  int action = ch - '0';
  Serial.print("\nReceived Input: ");
  Serial.println(action);
  validateAction(action);
}

int readSensor(void)
{
#ifndef TEST
  sensor.takeMeasurements();

  Serial.print(sensor.getCalibratedA()); // 410nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedB()); // 435nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedC()); // 460nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedD()); // 485nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedE()); // 510nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedF()); // 535nm
  Serial.print(",");

  Serial.print(sensor.getCalibratedG()); // 560nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedH()); // 585nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedR()); // 610nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedI()); // 645nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedS()); // 680nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedJ()); // 705nm
  Serial.print(",");

  Serial.print(sensor.getCalibratedT()); // 730nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedU()); // 760nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedV()); // 810nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedW()); // 860nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedK()); // 900nm
  Serial.print(",");
  Serial.print(sensor.getCalibratedL()); // 940nm
  Serial.print(",");

  Serial.println();
#endif
  return 0;
}

void promptUser(void)
{

  Serial.println("\nPress 1 to read sensor values.");
  Serial.println("Press 2 to read sensor values continuously upto 5 iterations.");
}

int validateAction(int action)
{
  switch (action)
  {
  case 1:
    Serial.println("\nA,B,C,D,E,F,G,H,R,I,S,J,T,U,V,W,K,L");
    readSensor();
    promptUser();
    break;
  case 2:
    Serial.println("\nA,B,C,D,E,F,G,H,R,I,S,J,T,U,V,W,K,L");
    for (int i = 0; i < 5; i++)
    {
      readSensor();
      delay(1000);
    }
    promptUser();
    break;
  default:
    Serial.println("Invalid action. Please try again.");
    promptUser();
    break;
  }
  return 0;
}